﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraZoomManagement : MonoBehaviour
{
    Camera thisCam;
    public float screenRatio;
    // Start is called before the first frame update
    private void Awake()
    {
        thisCam = this.GetComponent<Camera>();
    }
    void Start()
    {
        Debug.Log(thisCam.pixelWidth +""+ thisCam.pixelHeight);
        screenRatio = (float)thisCam.pixelWidth / (float)thisCam.pixelHeight;
        Debug.Log(screenRatio);

        if(screenRatio < (9f / 16f))
        {
            float fieldOfView = 40f * (9f / 16f) / (screenRatio);
            float diff = fieldOfView - 40;
            diff = diff * 1f;
            fieldOfView = 40 + diff;
            thisCam.fieldOfView = fieldOfView;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
