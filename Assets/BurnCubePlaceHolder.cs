﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniBliss;
public class BurnCubePlaceHolder : MonoBehaviour
{
    public GameObject burnCubeRef;
    public GameObject visualization;

    GameObject instanceOfCube;
    // Start is called before the first frame update
    void Awake()
    {
        instanceOfCube = Pool.Instantiate(burnCubeRef, this.transform.position, this.transform.rotation);
        instanceOfCube.GetComponent<burnCube>().ResetCube();
        visualization.SetActive(false);
    }

    // Update is called once per frame
    void OnDestroy()
    {
        Pool.Destroy(instanceOfCube);
    }
}
