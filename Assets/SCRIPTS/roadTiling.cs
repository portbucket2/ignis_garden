﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roadTiling : MonoBehaviour
{
    public float scale;
    public GameObject roadUnit;
    // Start is called before the first frame update
    void Start()
    {
        scale = this.transform.localScale.x;
        roadUnit.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1, scale);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
