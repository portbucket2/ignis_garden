﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endOfBurn : MonoBehaviour
{
    public bool atEnd;
    public int blastSize;
    public float blastScale;

    public GameObject ballOne;
    public GameObject ballTwo;

    public bool gotNext;

    //public bool decrease;
    // Start is called before the first frame update
    void Start()
    {
        blastScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        EndBlast();
    }


    void EndBlast()
    {
        if (atEnd)
        {
            //if(blastScale <= 0)
            //{
            //    decrease = false;
            //}
            //else if(blastScale >= blastSize)
            //{
            //    decrease = true;
            //}
            //
            //if (decrease)
            //{
            //    blastScale -= Time.deltaTime * 5f;
            //}
            //else
            //{
            //    blastScale += Time.deltaTime * 5f;
            //}
            if (blastScale >= blastSize)
            {
                blastScale += 0;
                
            }
            else
            {
                blastScale += Time.deltaTime * 5f;
            }
                
            this.transform.localScale = new Vector3(blastScale, blastScale, blastScale);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "burnBlock" && Vector3.Distance(this.transform.position, other.transform.position) > 1)
        {
            //this.gameObject.SetActive(false);
            if (!gotNext)
            {
                Debug.Log("gotNext");
                gotNext = true;
            }
            
            //other.transform.gameObject.GetComponentInParent<burningManagement>().inRangeBlock = other.gameObject;
        }
    }
}
