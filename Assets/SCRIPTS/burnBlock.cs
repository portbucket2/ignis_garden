﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class burnBlock : MonoBehaviour
{
    public float burnValue;

    public bool burnIt;
    // Start is called before the first frame update
    void Start()
    {
        burnValue = -1.1f;
    }

    // Update is called once per frame
    void Update()
    {
        burnValue = Mathf.Clamp(burnValue, -1.1f, 1);
        //this.GetComponent<Material>().SetFloat("burnValue", burnValue);
        this.GetComponent<Renderer>().material.SetFloat("Vector1_B4867CA0", burnValue);
        burnItself();
    }

    void burnItself()
    {
        if (burnIt)
        {
            burnValue += Time.deltaTime*3.5f;
        }
    }
}
