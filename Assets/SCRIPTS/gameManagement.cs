﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManagement : MonoBehaviour
{
    public LayerMask burnLayer;
    public static bool gameON;
    public static bool runPerson;
    public bool runPerson_ ;

    public bool clickValid;

    public static int cubesInScene;
    public int cubesInScene_;
    public int cubesInSceneDetector;

    public float impactRadius;

    //public GameObject aeroplane;
    public GameObject targetParticle;

    public GameObject UIcanavas;

    RaycastHit hit;

    public static int level;
    public int level_;

    public static int levelUnlocked;
    public int levelUnlocked_;

    public static int maxLevel;

    public float levelEndTimer;

    public bool GameOver;
    public int tutorialDone;

    public GameObject tutorialParticle;



    // Start is called before the first frame update
    void Awake()
    {
        cubesInScene = 0;
        UIcanavas.SetActive(true);
        maxLevel = 20;
    }
    void Start()
    {
        //PlayerPrefs.SetInt("tutorialDone", 0);
        

        level = PlayerPrefs.GetInt("level", 1);
        levelUnlocked = PlayerPrefs.GetInt("levelUnlocked", 1);

        tutorialDone = PlayerPrefs.GetInt("tutorialDone");
        if (tutorialDone == 0 && level == 1)
        {
            tutorialParticle.SetActive(true);
        }
        else
        {
            tutorialParticle.SetActive(false);
        }
        //level = 1;
        //levelUnlocked = 10;

        //cubesInSceneDetector = cubesInScene;
        //aeroplane.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        cubesInScene_ = cubesInScene;
        runPerson_ = runPerson;
        if (Input.GetMouseButtonDown(0) && clickValid == true)
        {
            RaycastSelect();
            
            
        }
        level_ = level;
        levelUnlocked_ = levelUnlocked;
        DetectGameEnd();
        UnlockLevel();
    }

    void RaycastSelect()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        //if (Physics.Raycast(ray, out hit, 500f, burnLayer)  && !gameON)
        //{
        //    Invoke("HitIt",0.8f);
        //    
        //    aeroplane.SetActive(true);
        //    aeroplane.transform.position = hit.transform.position;
        //}
        if (Physics.SphereCast(ray,impactRadius, out hit, 500f, burnLayer) && !gameON)
        {
            Invoke("HitIt", 0.5f);

            PlayerPrefs.SetInt("tutorialDone", 1);
            tutorialParticle.SetActive(false);

            //aeroplane.SetActive(true);
            //aeroplane.GetComponent<Animator>().SetTrigger("GoPlane");
            //aeroplane.transform.position = hit.transform.position;

            GameObject go =  Instantiate(targetParticle, hit.transform.position, Quaternion.identity);
            Destroy(go, 1.2f);
            
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
    public void PlayGame()
    {
        uIManager.isMenu   = false ;
        uIManager.isGame   = true ;
        uIManager.isGameEnd= false ;
        uIManager.isLevels = false;

        //clickValid = true;
        Invoke("MakeClickValid", 0.1f);
    }
    public void GoToLevels()
    {
        uIManager.isMenu = false;
        uIManager.isGame = false;
        uIManager.isGameEnd = false;
        uIManager.isLevels = true;

        clickValid = false;
    }
    public void GoToMenu()
    {
        uIManager.isMenu = true;
        uIManager.isGame = false;
        uIManager.isGameEnd = false;
        uIManager.isLevels = false;

        clickValid = false;
    }
    public void GoToGameEnd()
    {
        uIManager.isMenu = false;
        uIManager.isGame = false;
        uIManager.isGameEnd = true;
        uIManager.isLevels = false;

        clickValid = false;
    }

    public void RestartGame()
    {
        //aeroplane.GetComponent<Animator>().SetTrigger("ResetPlane");
        GameOver = false;
        //cubesInSceneDetector = cubesInScene;

        gameON = false;
        runPerson = false;
        UnlockLevel();



    }

    public void HitIt()
    {
        hit.transform.gameObject.GetComponent<burnCube>().active = true;
        gameON = true;
        runPerson = true;
        
    }


    void DetectGameEnd()
    {
        if (gameON && !GameOver)
        {
            if(levelEndTimer > 1.5f)
            {
                GameOver = true;
                GoToGameEnd();
                this.GetComponent<uIManager>().DeterminePanels();
                levelEndTimer = 0;
                if(cubesInScene <= 0)
                {
                    gameEndPanelManager.levelFailed = false;
                    Debug.Log("Achieved Level" + gameManagement.level);
                }
                else
                {
                    gameEndPanelManager.levelFailed = true;
                    Debug.Log("Level Failed" + gameManagement.level);
                }
            }
            else
            {
                levelEndTimer += Time.deltaTime;
            }
        }

        if(cubesInSceneDetector > cubesInScene)
        {
            levelEndTimer = 0;
            cubesInSceneDetector = cubesInScene;
        }
        else if(cubesInSceneDetector <= cubesInScene)
        {
            cubesInSceneDetector = cubesInScene;
        }
    }

    public void MakeClickValid()
    {
        clickValid = true;
    }

    public void UnlockLevel()
    {
        if(level > levelUnlocked)
        {
            levelUnlocked = level;
            PlayerPrefs.SetInt("levelUnlocked", levelUnlocked);
        }
    }


    public void ResetGame()
    {
        PlayerPrefs.SetInt("level", 1);
        PlayerPrefs.SetInt("levelUnlocked", 1);
        PlayerPrefs.SetInt("tutorialDone", 0);
    }

    

    //public void UpdateCubesInSceneDetector()
    //{
    //    cubesInSceneDetector = cubesInScene;
    //}
}
