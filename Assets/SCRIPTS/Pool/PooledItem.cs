﻿using UnityEngine;
using System.Collections.Generic;
namespace UniBliss
{
    public class PooledItem : MonoBehaviour
    {
        public bool alive = true;
        public int useCount = 0;
        public GameObject original;

        void OnDestroy() { if (alive) UniBliss.Pool.Destroy(gameObject); }

        void Awake()
        {
            if (!alive && original != null)
            {
                Pool.RegisterToPool(this);
            }
        }

        //bool initialized = false;

        //public void Init()
        //{
        //    if (initialized) return;


        //}
    }

}