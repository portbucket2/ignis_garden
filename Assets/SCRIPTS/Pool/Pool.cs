﻿using UnityEngine;
using System.Collections.Generic;


namespace UniBliss
{
    public static class Pool
    {

        private static Dictionary<GameObject, List<GameObject>> poolDictionary = new Dictionary<GameObject, List<GameObject>>();
        private static Dictionary<GameObject, GameObject> prefabMap = new Dictionary<GameObject, GameObject>();
        //public static Vector3 defaultPosition = Vector3.zero;
        //public static Quaternion defaultRotation = Quaternion.identity;

        public static int poolCount = 0;

        public static Transform UnmanagedPooledObjectParent;


        public static void PrePopulatePool(GameObject original, PooledItem[] prePopulation)
        {
            if (!poolDictionary.ContainsKey(original))
            {
                poolDictionary.Add(original, new List<GameObject>());
                counting.Add(original, 0);
                poolCount++;
            }

            List<GameObject> currentPool = poolDictionary[original];
            if (prePopulation == null)
            {
                Debug.LogError("Pre-population array is null!");
            }
            GameObject go;
            foreach (PooledItem ppitem in prePopulation)
            {
                if (ppitem == null)
                {
                    Debug.LogError("Pre pooled item is null");
                    continue;
                }
                go = ppitem.gameObject;
                ppitem.original = original;
                //ppitem.prepopulationParent = ppitem.transform.parent;
                prefabMap.Add(go, original);
                currentPool.Add(go);
                go.SetActive(false);
                ppitem.alive = false;

            }
        }

        public static void RegisterToPool(PooledItem ppitem)
        {
            GameObject original = ppitem.original;

            if (!poolDictionary.ContainsKey(original))
            {
                poolDictionary.Add(original, new List<GameObject>());
                counting.Add(original, 0);
                poolCount++;
            }

            List<GameObject> currentPool = poolDictionary[original];

            GameObject go;

            if (ppitem == null)
            {
                Debug.LogError("Pre pooled item is null");
                return;
            }
            go = ppitem.gameObject;
            //ppitem.prepopulationParent = ppitem.transform.parent;
            prefabMap.Add(go, original);
            currentPool.Add(go);
            go.SetActive(false);

        }


        public static GameObject Instantiate(GameObject original, Transform parent = null)
        {
            return Instantiate(original, Vector3.zero, Quaternion.identity, parent);
        }


        public static Dictionary<GameObject, int> counting = new Dictionary<GameObject, int>();

        public static GameObject Instantiate(GameObject original, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (!poolDictionary.ContainsKey(original))
            {
                poolDictionary.Add(original, new List<GameObject>());
                counting.Add(original, 0);
                poolCount++;
            }
            if (UnmanagedPooledObjectParent == null)
            {
                UnmanagedPooledObjectParent = (new GameObject("PoolManager")).transform;
                //UnmanagedPooledObjectParent.gameObject.AddComponent<PoolManager>();
            }
            List<GameObject> currentPool = poolDictionary[original];
            CleanPool(currentPool);

            if (currentPool.Count == 0)
            {
                counting[original] = counting[original] + 1;
                //Debug.LogFormat("{0}-{1}",original.name, counting[original]);
                GameObject go = MonoBehaviour.Instantiate(original, position, rotation) as GameObject;
                if (parent == null)
                {
                    parent = UnmanagedPooledObjectParent;
                }
                if (go.transform.parent != parent)
                {
                    go.transform.SetParent(parent);
                }
                PooledItem pitem = go.AddComponent<PooledItem>();
                pitem.original = original;
                pitem.alive = true;
                pitem.useCount++;
                prefabMap.Add(go, original);
                return go;
            }
            else
            {
                GameObject go = currentPool[currentPool.Count - 1];
                currentPool.RemoveAt(currentPool.Count - 1);
                if (go == null) Debug.LogError("isnull");
                Transform tr = go.transform;
                PooledItem pitem = go.GetComponent<PooledItem>();
                tr.SetParent(UnmanagedPooledObjectParent);
                if (pitem.alive) Debug.LogErrorFormat("Pool Error On - {0}", pitem.name);
                pitem.useCount++;
                go.GetComponent<PooledItem>().alive = true;
                tr.position = position;
                tr.rotation = rotation;
                go.SetActive(true);
                return go;
            }
        }
        public static bool IsPooled(GameObject gameObject)
        {
            return prefabMap.ContainsKey(gameObject);
        }
        public static void Destroy(GameObject gameObject, bool shouldTrash = false)
        {
            if (gameObject == null)
            {
                ////CM_Deb"Pool.Destroy was called with a destroyed game object...");
                return;
            }
            else if (!prefabMap.ContainsKey(gameObject))
            {
                ////CM_Deb"Pool.Destroy was called with an unpooled object...");
                MonoBehaviour.Destroy(gameObject);
                return;
            }


            GameObject original = prefabMap[gameObject];
            /*should not be necessary
             * if (!poolDictionary.ContainsKey(original))
            {
                poolDictionary.Add(original, new List<GameObject>());
                poolCount++;
            }*/
            List<GameObject> currentPool = poolDictionary[original];
            PooledItem pitem = gameObject.GetComponent<PooledItem>();
            if (!pitem.alive)
            {
                Debug.LogWarningFormat("Tried to destroy an object twice - {0}", pitem.name);
                return;
            }
            //else { //CM_Deb"{0} pool item destroyed", pitem.name); }
            pitem.alive = false;
            gameObject.SetActive(false);
            currentPool.Add(gameObject);

            if (shouldTrash)
            {
                if (UnmanagedPooledObjectParent == null)
                    UnmanagedPooledObjectParent = (new GameObject("PoolManager")).transform;

                gameObject.transform.SetParent(UnmanagedPooledObjectParent);
            }
        }


        private static void CleanPool(List<GameObject> currentPool)
        {
            for (int i = currentPool.Count - 1; i >= 0; i--)
            {
                if (currentPool[i] == null) currentPool.RemoveAt(i);
            }
        }
        //public static void ReleasePool(GameObject original)
        //{
        //    if (poolDictionary.ContainsKey(original))
        //    {
        //        List<GameObject> currentPool = poolDictionary[original];
        //        for (int i = 0; i < currentPool.Count; i++)
        //        {
        //            prefabMap.Remove(currentPool[i]);
        //            MonoBehaviour.Destroy(currentPool[i]);
        //        }
        //        poolDictionary.Remove(original);
        //        poolCount--;
        //    }
        //}
    }

}