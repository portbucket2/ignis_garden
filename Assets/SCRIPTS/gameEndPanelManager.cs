﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameEndPanelManager : MonoBehaviour
{
    public static bool levelFailed;
    public bool levelFailed_;

    public GameObject faileurePanel;
    public GameObject successPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        levelFailed_ = levelFailed;
        if (levelFailed)
        {
            faileurePanel.SetActive(true) ;
            successPanel.SetActive(false);
        }
        else
        {
            faileurePanel.SetActive(false);
            successPanel.SetActive(true);
        }
    }

    //private void OnEnable()
    //{
    //    if (levelFailed)
    //    {
    //        faileurePanel.SetActive(true);
    //        successPanel.SetActive(false);
    //    }
    //    else
    //    {
    //        faileurePanel.SetActive(false);
    //        successPanel.SetActive(true);
    //    }
    //}

    private void OnDisable()
    {
        faileurePanel.SetActive(false);
        successPanel.SetActive(false);
    }
}
