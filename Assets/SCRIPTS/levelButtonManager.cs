﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelButtonManager : MonoBehaviour
{
    public bool isLocked;

    public int levelOwn;
    public Text levelText;
    public GameObject lockedImage;

    public int level;
    public int levelUnlocked;
    public Button thisButton;
    // Start is called before the first frame update
    void Start()
    {
        levelUnlocked = gameManagement.levelUnlocked;
        
        //levelText.text = "" + levelOwn;
        UnlockIt();
    }

    // Update is called once per frame
    void Update()
    {
        //thisButton.onClick.AddListener(OnClickOwn);
        levelUnlocked = gameManagement.levelUnlocked;
        UnlockIt();
        if (isLocked)
        {
            thisButton.enabled = false;
        }
        else
        {
            thisButton.enabled = true;
        }
        
            
    }

    public void UnlockIt()
    {
        if (levelOwn <= levelUnlocked)
        {
            isLocked = false;
        }
        else
        {
            isLocked = true;
        }

        if (isLocked)
        {
            levelText.gameObject.SetActive(false);
            lockedImage.SetActive(true);
        }
        else
        {
            levelText.gameObject.SetActive(true);
            lockedImage.SetActive(false);
        }
        levelText.text = "" + levelOwn;
    }

    

    public void OnClickOwn()
    {
        gameManagement.level = levelOwn;
        uIManager.doUiUpdate = true;
        
    }

    
    //public void GoToCertainLevel()
    //{
    //    gameManagement.level += 1;
    //    uIManager.doUiUpdate = true;
    //}
}
