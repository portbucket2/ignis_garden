﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadLevels : MonoBehaviour
{
    //public GameObject[] levelsPrefab;
    public GameObject currentLevel;
    public Vector3[] cameraPoses;
    public Vector3 currentCamPos;
    public GameObject cam;

    public GameObject[] arenas;

    public Color[] flowerColors;
    public Material[] flowerMats;
    public Material[] tobMats;
    public Color[] tobColors;

    public  static int flowerIndex1;
    public static int flowerIndex2;

    public int colorIndex;

    public int flowerMatColorIndex1;
    public int flowerMatColorIndex2;


    //public GameObject paneloutOfLevels;
    // Start is called before the first frame update
    private void Awake()
    {
        //currentLevel = levelsPrefab[gameManagement.level - 1];
    }

    void Start()
    {
        //LoadLevel();
        //cam = Camera.main;
        //currentLevel = levelsPrefab[gameManagement.level - 1];
        //currentLevel = Instantiate(currentLevel);
        currentCamPos = cam.transform.position;
        //paneloutOfLevels.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCameraPos();
    }

    public void LoadLevel()
    {
        DetermineFolwerColor();
        AppearArena();

        if (currentLevel)
            Destroy(currentLevel.gameObject);
        gameManagement.cubesInScene = 0;
        currentLevel = Instantiate( Resources.Load<GameObject>(string.Format("level{0}", gameManagement.level )));//levelsPrefab[gameManagement.level-1 ]);
        PlayerPrefs.SetInt("level", gameManagement.level);

        Debug.Log("Level Started"+ gameManagement.level);
        //currentLevel = 
    }

    public void UpdateNextLevel()
    {
        gameManagement.level += 1;

        if(gameManagement.level > gameManagement.maxLevel)
        {
            gameManagement.level -= 1;
            uIManager.isOutOfLevels = true;
            //paneloutOfLevels.SetActive(true);
        }
        else
        {
            uIManager.doUiUpdate = true;
            PlayerPrefs.SetInt("level", gameManagement.level);
        }
        
    }
    

    public void UpdateCameraPos()
    {
        currentCamPos = Vector3.Lerp(currentCamPos, cameraPoses[gameManagement.level - 1], Time.deltaTime * 5f);
        cam.transform.position = currentCamPos;
    }

    public void AppearArena()
    {
        if(gameManagement.level <= 10)
        {
            arenas[0].SetActive(true);
            arenas[1].SetActive(false);
        }
        else
        {
            arenas[1].SetActive(true);
            arenas[0].SetActive(false);
        }
    }

    public void DetermineFolwerColor()
    {
        //int colorIndex;
        colorIndex = (gameManagement.level) - 1;
        //if (gameManagement.level > 10)
        //{
        //    if (gameManagement.level % 10 == 0)
        //    {
        //        colorIndex = 9;
        //    }
        //    else
        //    {
        //        colorIndex = (gameManagement.level % 10) - 1;
        //    }
        //
        //    
        //}
        //else
        //{
        //    colorIndex = (gameManagement.level) - 1;
        //}

        //int flowerMatColorIndex1 ;
        //int flowerMatColorIndex2 ;
        //if (gameManagement.level > 10)
        //{
        //    flowerMatColorIndex1 =20 + colorIndex * 2;
        //    flowerMatColorIndex2 = 20 + colorIndex * 2 + 1;
        //}
        //else
        //{
        //   flowerMatColorIndex1 = colorIndex * 2;
        //   flowerMatColorIndex2 = colorIndex * 2 + 1;
        //}

        flowerMatColorIndex1 = colorIndex ;
        flowerMatColorIndex2 = colorIndex  + 1;

        //flowerMatColorIndex1 = colorIndex * 2;
        //flowerMatColorIndex2 = colorIndex * 2 + 1;




        //string str = "tobColors" + 1;
        tobMats[0].color = tobColors[ colorIndex *3 ];
        tobMats[1].color = tobColors[colorIndex * 3  +1];
        tobMats[2].color = tobColors[colorIndex * 3  +2];

        if(gameManagement.level < 6)
        {
            flowerIndex1 = gameManagement.level;
            flowerIndex2 = gameManagement.level + 1;
        }
        else if (gameManagement.level >= 6)
        {
            flowerIndex1 = gameManagement.level % 6;
            flowerIndex2 = (gameManagement.level % 6) + 1;
        }

        flowerMats[flowerIndex1].color = flowerColors[flowerMatColorIndex1];
        flowerMats[flowerIndex2].color = flowerColors[flowerMatColorIndex2];

    }
}
